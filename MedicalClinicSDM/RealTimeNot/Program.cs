﻿using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace RealTimeNot
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:/Users/Liviu/source/repos/MedicalClinicSDM/RealTimeNot/activity.txt";

            List<Test> test = new List<Test>();
            string[] data = System.IO.File.ReadAllLines(path);
            foreach (string s in data)
            {
                test.Add(new Test {PacientID = 1, StartDate = s.Split("\t\t")[0], EndDate = s.Split("\t\t")[1], Activity = Regex.Replace(s.Split("\t\t")[2], @"\t|\n|\r", "")});
            }

            foreach(Test tt in test)
            {
                if (tt.Activity.Equals("Sleeping"))
                {
                    DateTime sdate = DateTime.Parse(tt.StartDate);
                    DateTime edate = DateTime.Parse(tt.EndDate);
                    if ((edate - sdate).TotalHours >= 8)
                    {
                        Console.WriteLine("Problem with sleep");
                    }
                }
                if (tt.Activity.Equals("Leaving"))
                {
                    DateTime sdate = DateTime.Parse(tt.StartDate);
                    DateTime edate = DateTime.Parse(tt.EndDate);
                    if ((edate - sdate).TotalHours >= 4)
                    {
                        Console.WriteLine("Problem with leaving");
                    }
                }
                if (tt.Activity.Equals("Toileting"))
                {
                    DateTime sdate = DateTime.Parse(tt.StartDate);
                    DateTime edate = DateTime.Parse(tt.EndDate);
                    if ((edate - sdate).TotalHours >= 1)
                    {
                        Console.WriteLine("Problem with toilet");
                    }
                }
            }

            var json = JsonConvert.SerializeObject(test);
            //Console.WriteLine(json);
        }
    }
}
