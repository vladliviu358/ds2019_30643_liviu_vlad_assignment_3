﻿using MedicalClinicSDM.Data;
using MedicalClinicSDM.Models;
using MedicalClinicSDM.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Controllers
{
    public class PillDispenserController : Controller
    {
        private readonly MedicalClinicSDMContext _context;
        private readonly ILogger<PillDispenserController> _logger;
        public PillDispenserController(MedicalClinicSDMContext context, ILogger<PillDispenserController> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            PillDispenserViewModel pd;
            List<PillDispenserViewModel> pdl = new List<PillDispenserViewModel>();


            var caregiverPacients = _context.CaregiverPacient.Include(c => c.Caregiver).Include(p => p.Pacient).Where(c => c.CaregiverID == HttpContext.Session.GetInt32("UserID"));
            foreach (var c in caregiverPacients)
            {
                var medicationPacients = _context.MedicationPacient.Include(m => m.Medication).Include(p => p.Pacient).Where(p => p.PacientID == c.PacientID);
                foreach (var m in medicationPacients)
                {
                    var med = _context.Medication.Where(mm => mm.ID == m.MedicationID).First();
                    var pac = _context.Pacient.Where(pp => pp.ID == m.PacientID).First();

                    pd = new PillDispenserViewModel();
                    pd.MedicationID = med.ID;
                    pd.PacientID = pac.ID;
                    pd.PacientName = pac.Name;
                    pd.Dosage = med.Dosage;
                    pd.MedicationName = med.Name;
                    pd.TakenFrom = med.TakeFrom;
                    pd.TakenTo = med.TakeTo;
                    pd.Taken = med.Taken;
                    if (med.TakeTo < DateTime.Now)
                    {
                        pd.FlagNotTaken = true;
                    }
                    else
                    {
                        pd.FlagNotTaken = false;
                    }
                    pdl.Add(pd);
                }
            }

            return View(pdl);
        }

        [HttpPost, ActionName("Take")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Take(int medicationID)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");

            var medication = await _context.Medication.FindAsync(medicationID);
            medication.Taken = true;
            medication.TakeFrom = medication.TakeFrom.AddDays(1);
            medication.TakeTo = medication.TakeTo.AddDays(1);

            _context.Update(medication);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "PillDispenser");
        }

        [HttpPost, ActionName("ClearTaken")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ClearTaken()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");

            var medication = _context.Medication.Where(t => t.Taken == true).ToList();
            foreach (Medication m in medication)
            {
                m.Taken = false;
                m.TakeFrom = m.TakeFrom.AddDays(-1);
                m.TakeTo = m.TakeTo.AddDays(-1);
                _context.Update(m);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index", "PillDispenser");
        }

        public async void DownloadMedicationPlan()
        {
            var logPath = "C:/Users/Liviu/Desktop/Tema3SD";
            var logFile = System.IO.File.Create(logPath);
            var logWriter = new System.IO.StreamWriter(logFile);
            logWriter.WriteLine("Log message");
            logWriter.Dispose();
        }


        private bool MedicationExists(int id)
        {
            return _context.Medication.Any(e => e.ID == id);
        }

    }
}
